<?php include('header-page.php'); ?>
<div class="body">

  <!-- Select Model -->
  <section class="tag-section">
    <div class="container">
      <div class="select-model">
        <div class="text-center text-uppercase">
          <h3>
            shop used rolex watches for sale
          </h3>
        </div>
        <!-- pagination -->
        <div class="pt-3">
          <!-- this the pagination buttons -->
          <div class="tags">
            <ul class="text-uppercase  d-flex flex-row list-unstyled">
              <li><a href="#submariner">Seamaster</a></li>
              <li><a href="#rolex-16610">Speedmaster</a></li>
              <li><a href="#datejust">aqua tera</a></li>
              <li><a href="#daytona">planet ocean</a></li>
              <li><a href="#day-date">constelation</a></li>
              <li><a href="#president">de ville</a></li>
              <li><a href="#gmt-master">vintage</a></li>
            </ul>
          </div>

        </div>

        <!-- product found -->

        <div
          class="row d-flex justify-content-xxl-between justify-content-xl-between justify-content-xl-between align-items-center">
          <div class="col-xxl-3 col-xl-3 col-md-6 col-sm-6 col-12 text-center">
            <p><strong><span>84 </span>product found</strong></p>
          </div>
          <div class="col-xxl-3 col-xl-3 col-md-6- col-sm-6 col-12 text-center">
            <p><a href="#sellMyRolex">Sell my rolex</a></p>
          </div>

        </div>
      </div>
      <hr>
  </section>

  <!-- product and filter nav-->
  <section class="products p-0">

    <!-- Side Bar -->
    <div class="container">
      <div class="row">

        <div class="input-group search-group" id="search-group">
          <div class="col-12 d-flex align-items-center justify-content-start">
            <ul>
              <li class="search-bar">
                <input type="text" class="text" placeholder="Search Within Result">
                <input type="button" class="btn search-btn">
              </li>
              <li>
                <label class="label-for-page" for="">Per Page</label>
                <select class="per-page" id="sel1">
                  <option>30</option>
                  <option>60</option>
                  <option>90</option>
                </select>
              </li>
              <li>

                <label class="label-for-sort" for="">Sort By</label>
                <select class="sort-by" id="sel1">
                  <option>Featured</option>
                  <option>Newest to oldest</option>
                  <option>Price - Low to Hight</option>
                  <option>Price - High to Low</option>
                </select>
              </li>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- side nav -->



  <section class="poduct-component">

    <div class="container">
      <div class="side-bar" id="side-bar">
        <div class="compare-watches mb-2">
          <a class="side" href="#"> Compare Watches</a>
        </div>
        <!--new/used-->

        <div class="filter-nav py-2">
          <h6 class="text-uppercase">New/Used</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
              Used
            </label>
            <span>(474)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
              New
            </label>
            <span>(213)</span>
          </div>
        </div>
        <!--end-new-/used-->

        <!--Size-->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Size</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">44mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">42mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">41mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">39mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">38mm</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#size">+View More</a>

          <div id="size" class="collapse">

            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">37mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">36mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">34mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">31mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">29mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">28mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">26mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">24mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">15mm</label><span>(0)</span>
            </div>
          </div>
        </div>
        <!-- end-size  -->
        <!-- metal-type -->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Metal Type</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Stainless Steel</label><span>(298)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Steel and Gold</label><span>(111)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Yellow Gold</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">White Gold</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Rose Gold</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#metal-type">+View More</a>

          <div id="metal-type" class="collapse">

            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Steel and Rose Gold</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Titanium</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Platinum</label><span>(0)</span>
            </div>

          </div>
        </div>
        <!--end-metal-type-->
        <!-- price -->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Price</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Under $2,000</label><span>(298)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$2,000 - 5,000</label><span>(111)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$5,000 - 7,000</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$7,000 - 10,000</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$10,000 - 15,000</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#price">+View More</a>

          <div id="price" class="collapse">

            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">$15,000 - 20,000</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Over $20,000</label><span>(0)</span>
            </div>
          </div>
        </div>
        <!-- end-price -->
        <!-- Color -->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Color</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline black" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Black</label><span>(298)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Champagne</label><span>(111)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Silver</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">White</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Blue</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#color">+View More</a>

          <div id="color" class="collapse">
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Diamonds</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">State</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Green</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Brown</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Ivory</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Rose</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Red</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Mother of Pearl</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Purple</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Wood</label><span>(0)</span>
            </div>
          </div>
        </div>
      </div>

      <div class="more-filter">
        <div class="container">
          <div class="row">
            <div class="col">
              <input type="button" class="more-filter-btn" id="moreFilter" value="More Filter">
            </div>
          </div>
        </div>
      </div>


      <!-- prodcut list -->
      <div class="productlist">
        <div class="container">

          <div class="row product-main justify-content-center align-items-center">
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-2.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Omega Speedmaster The Met Edition</a></h2>
                <p>Black index Dial, Red/White NATO Stainless Steel, B&P</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-3.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Omega Constellation Diamonds</a></h2>
                <p>Mother of Pearl Diamond Dial Stainless Steel Bracelet, B&P</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>

            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-4.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Omega Seamaster Bullhead Black</a></h2>
                <p>Black Dial, Stainless Steel Black Leather Strap, B&P (2014)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-5.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Omega Seamaster Aqua Terra</a></h2>
                <p>Grey Teak Dial, Gold & Red Gold Retail $14,400 (46% OFF)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-1.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Mens Omega Seamaster Diver</a></h2>
                <p>Stainless Steel & 18k Sedna Gold Ceramic Wave Dial, B&P (2019)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-2.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Mens Omega Seamaster Diver 300M</a></h2>
                <p>Titanium & 18k Sedna Gold Model Grey Wave Dial, B&P (2020)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-3.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Omega Seamaster 300M</a></h2>
                <p>Brown Ceramic Timing Bezel Titanium Mesh Bracelet, B&P (2020)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-4.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Mens Omega Seamaster 007 Edition</a></h2>
                <p>Brown Tropical Dial, Ceramic Bezel Titanium Mesh Bracelet, B&P (2020)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-2.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Omega Seamaster Aqua Terra</a></h2>
                <p>18k Red Gold, Silver Teak Dial Retail $23,300 (45% OFF)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>

            <!-- End of Product List -->
            <!-- pagination -->
            <div class="col-12 pt-5">
              <div class="d-flex justify-content-center align-items-center">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>

            <!-- end of pagination -->

          </div>
        </div>
  </section>

  <div class="container">
    <div class="embed-responsive embed-responsive-4by3">
      <iframe class="embed-responsive-item rolex-video" src="https://www.youtube.com/embed/ASAe3UQUBoc?rel=0"
        allowfullscreen></iframe>
    </div>
  </div>



  <section class="text-content">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h3 class="text-uppercase text-center"> This page contains information about:</h3>
        </div>
      </div>

      <div class="row 1ign-items-center py-3">
        <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12 text-center p-1">
          <a href="">Omega History</a>
        </div>
        <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12 text-center p-1">
          <a href="">Price for Omega</a>
        </div>
        <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12 text-center p-1">
          <a href="">Speedmaster</a>
        </div>
        <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12 text-center p-1">
          <a href="">Seamaster</a>
        </div>
        <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12 text-center p-1">
          <a href=""> Omega Movement</a>
        </div>
      </div>

      <div class="row">

        <div class="col-12">
          <!-- about omega -->
          <h3 class="text-center">ABOUT OMEGA WATCHES</h3>

        </div>

        <div class="row py-5">
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <p>OMEGA wristwatches have developed a loyal following and for good reason. Their accuracy, functionality,
              and style have earned them the title of the official Olympic Games timekeeper, and they are worn and
              collected by some of the most influential people in the world. The brand appeals to a broad range of
              individuals with both men's and women's models available, and famous people who have worn these watches
              include John F. Kennedy, Prince William, and Buzz Aldrin.</p>
            <p>From the depths of the ocean to outer space, from timing the Olympic Games to equipping fictional super
              spies, the brand has been a part of some of history's most memorable moments. The companies history
              stretches </p>

          </div>
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <p>back to 1848 and over the course of 170 years, the Swiss watchmaker has produced a vast assortment of
              timepieces of which many have become luxury watch icons. Thanks to its popular watch models, Omega is now
              a globally renowned brand and is one of the top three Swiss luxury watch brands in the world by annual
              sales. Whether for men or women, sporty or dressy, simple or complicated, quartz, or mechanical, they
              manufacture watches for just about any type of watch enthusiast. Bob's Watches offers the best selection
              of new, used and pre-owned OMEGA watches with the best prices.</p>
          </div>
        </div>
      </div>
      <div class="row py-2">
        <div class="col-12">
          <center>
            <hr>
          </center>
        </div>
      </div>
      <!-- omega History -->
      <div class="col-12">
        <h3 class="text-uppercase text-center"> OMEGA HISTORY</h3>
      </div>
      <div class="row py-5">
        <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
          <p>In 1848, a 23-year-old watchmaker named Louis Brandt opened his workshop in La Chaux-de-Fonds, Switzerland,
            laying the foundation of what would eventually become Omega. It was in 1894, within a new and larger
            facility in the city of Bienne, that the Brandt sons, Louis Paul and Cesar, produced a groundbreaking
            19-ligne caliber that was accurate, simple to service, and combined time-setting and movement winding in one
            crown. The Brandt brothers named the revolutionary movement the "Omega" and the series-produced caliber was
            met with such success within the watch industry that the siblings decided to rename the company from "Louis
            Brandt & Fils" to "OMEGA."</p>
          <p>Thanks to the companies technological advances in horology, the company received the grand prize at the
            Universal Exposition in Paris in 1900, and by 1903, they had earned its spot as the largest manufacturer of
            finished Swiss watches. During the early-1900s, The company continued its focus on making precise, reliable,
            and easy-to-repair movements and watches. And in 1931, the the chronometer set precision records in all six
            trials at the Geneva Observatory. Later that decade, the company set another two precision records, this
            time at Kew-Teddington. Since the company was renowned for its precise timekeeping instruments, Omega became
            the first watchmaker appointed to time the entire Olympic games (1932, Los Angeles)—a tradition that carries
            on today.</p>
          <p>In 1932, they unveiled the Marine watch as a pioneering timepiece in the dive watch genre with a waterproof
            double case reinforced with cork and a clasp fitted with a diver's extension. The Marine was tested in Lake
            Geneva, </p>
        </div>
        <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
          <p>where it successfully dove down 73 meters deep. In the 1940s, the brand was the largest supplier of watches
            for the British armed forces and its allies during World War II.</p>
          <p>To celebrate the company's 100th anniversary and partly inspired by the rugged military watches it had made
            during WII, the brand introduced the now-famous Seamaster water-resistant watch in 1948. Just a few years
            later in 1952, another iconic watch made its debut—the Constellation. Named after the eight stars on its
            crest (representing the precision records set by Omega), the Constellation became their flagship dress
            watch. </p>
          <p>1957 is perhaps the most important year in their history. That year saw the birth of the Professional line
            of watches, which included three now-legendary timepieces: the Speedmaster chronograph, the Seamaster 300
            dive watch, and the Railmaster antimagnetic watch. A decade later, the De Ville moved away from the
            Seamaster lineup and branched off into its own collection of modern non-sport watches with top-tier
            movements.</p>
          <p>By the 1980s, much of the Swiss watch industry had been decimated by the Quartz Crisis. Through a series of
            bank directives, acquisitions, mergers, and negotiations by the late Nicolas Hayek throughout the decade,
            Omega eventually became a part of the conglomerate we now know as the Swatch Group.</p>
        </div>
      </div>
      <div class="row py-2">
        <div class="col-12">
          <center>
            <hr>
          </center>
        </div>


        <!-- MAIN COLLECTIONS AND SUB COLLECTIONS -->
        <div class="col-12">
          <div class="text-center">
            <h3 class="text-uppercase">MAIN COLLECTIONS AND SUB COLLECTIONS</h3>
          </div>
          <!-- new row -->
          <div class="row py-5">
            <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <p>Although the company makes an immense range of watch models, the brand now divides its lineup into four
                main collections: Speedmaster, Seamaster, Constellation, and De Ville. These collections then branch off
                into specific models such as the Speedmaster Moonwatch, Speedmaster Racing, Seamaster Diver 300M,
                Seamaster Planet Ocean, Seamaster Aqua Terra, Constellation Globemaster, De Ville Ladymatic, and so on.
              </p>
              <h4>SPEEDMASTER</h4>
              <p>Moonwatch Speedmaster '57 Mark II Racing Speedmaster 38 Speedmaster Solar Impulse HB-SIA X-33 Regatta
                Skywalker X-33 Spacemaster Z-33</p>
            </div>
            <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <h4>SEAMASTER</h4>
              <p>Diver 300M Aqua Terra 150M Seamaster 300 Railmaster Planet Ocean 1948 Bullhead PloProf 1200M</p>
              <h4>CONSTELLATION</h4>
              <p>Constellation Globemaster Constellation</p>
              <h4>DE VILLE</h4>
              <p>De Ville Ladymatic De Ville Hour Vision De Ville Tresor De Ville Prestige De Ville De Ville Tourbillon
              </p>
            </div>
          </div>
          <div class="row py-2">
            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>

            <!-- HOW MUCH DO OMEGA WATCHES COST? -->
            <div class="col-12">
              <div class="text-center py-3">
                <h3 class="text-uppercase">HOW MUCH DO OMEGA WATCHES COST?</h3>
              </div>
              Below you'll find a general price guide for some popular watch models. Naturally, the price of precious
              metal watches are higher than stainless steel models in the retail market; however, steel watches
              typically retain more of their original value when re-sold on the secondary market. Some limited-edition
              models sell pre-owned for higher than MSRP in the secondary market, which explains why in some cases the
              used price on watches for sale is higher than the retail price.
            </div>

            <!-- table -->
            <div class="col-12 pt-4">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Model</th>
                      <th scope="col">Reference</th>
                      <th scope="col">MSRP</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">Speedmaster Professional Moonwatch</th>
                      <td>311.30.42.30.01.005</td>
                      <td> $5,350</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster Professional "Speedy Tuesday Ultraman"</th>
                      <td> 311.12.42.30.01.001</td>
                      <td> $7,100</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster Professional "Dark Side of the Moon"</th>
                      <td>311.92.44.51.01.003</td>
                      <td> $12,000</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster "First OMEGA In Space"</th>
                      <td>311.63.40.30.02.001</td>
                      <td> $18,000</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster Professional Apollo 11 50th Anniversary</th>
                      <td>310.60.42.50.99.001</td>
                      <td> $34,600</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster '57</th>
                      <td> 331.20.42.51.01.002</td>
                      <td> $7,250</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster '57</th>
                      <td> 331.20.42.51.01.002</td>
                      <td> $15,000</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster Racing</th>
                      <td>326.30.40.50.06.001</td>
                      <td> $4,800</td>

                    </tr>
                    <tr>
                      <th scope="row">Speedmaster 38</th>
                      <td> 324.30.38.50.01.001</td>
                      <td> $5,100</td>

                    </tr>
                    <tr>
                      <th scope="row">Seamaster Diver 300M</th>
                      <td> 210.30.42.20.01.001</td>
                      <td> $5,200</td>

                    </tr>
                    <tr>
                      <th scope="row">Seamaster Diver 300M</th>
                      <td>210.20.42.20.01.001</td>
                      <td> $9,700</td>

                    </tr>
                    <tr>
                      <th scope="row">Seamaster Diver 300M Chronograph</th>
                      <td> 210.30.44.51.01.001</td>
                      <td> $7,450</td>

                    </tr>
                    <tr>
                      <th scope="row">Seamaster 300</th>
                      <td> 233.30.41.21.01.001</td>
                      <td> $6,800</td>

                    </tr>
                    <tr>
                      <th scope="row">Aqua Terra 150M</th>
                      <td> 220.10.41.21.01.001</td>
                      <td> $5,700</td>

                    </tr>
                    <tr>
                      <th scope="row">Aqua Terra 150M GMT</th>
                      <td> 231.10.43.22.03.001</td>
                      <td> $7,900</td>

                    </tr>
                    <tr>
                      <th scope="row">Railmaster</th>
                      <td> 220.10.40.20.01.001</td>
                      <td> $5,200</td>

                    </tr>
                    <tr>
                      <th scope="row">Planet Ocean 600M</th>
                      <td> 215.30.44.22.01.001</td>
                      <td> $6,550</td>

                    </tr>
                    <tr>
                      <th scope="row">Planet Ocean 600M GMT</th>
                      <td> 215.30.44.21.01.001</td>
                      <td> $8,000</td>

                    </tr>
                    <tr>
                      <th scope="row">Planet Ocean 600M Chronograph</th>
                      <td> 215.30.46.51.01.001</td>
                      <td> $8,450</td>

                    </tr>
                    <tr>
                      <th scope="row">PloProf 1200M</th>
                      <td> 227.90.55.21.01.001</td>
                      <td> $12, 600</td>
                    </tr>
                    <tr>
                      <th scope="row">Globemaster Master Chronometer</th>
                      <td> 130.53.39.21.02.001</td>
                      <td>$19,800</td>
                    </tr>
                    <tr>
                      <th scope="row">Globemaster Annual Calendar</th>
                      <td>130.33.41.22.06.001</td>
                      <td> $8,600</td>
                    </tr>
                    <tr>
                      <th scope="row">De Ville Hour Vision Annual Calendar</th>
                      <td>433.13.41.22.02.001</td>
                      <td> $10,600</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- end of table -->
            <div class="row py-2">
              <div class="col-12">
                <center>
                  <hr>
                </center>
              </div>
              <!-- WATCH PRICE INFORMATION -->
              <div class="col-12 py-3">
                <div class="text-center">
                  <h3 class="text-uppercase">WATCH PRICE INFORMATION</h3>
                </div>
              </div>

              <!-- new row -->
              <div class="row py-5">
                <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <p>Omega watches are much more affordable luxury Swiss timepieces with prices starting around $2,500
                    for entry-level models on the secondary .</p>
                </div>
                <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <h4 class="text-uppercase">craftsmanship</h4>
                  <p>market. The retail price for a Speedmaster Moonwatch Professional Chronograph 42 MM, steel on steel
                    311.30.42.30.01.005 is currently $5,350.</p>
                </div>
              </div>

              <div class="row py-2">
                <div class="col-12">
                  <center>
                    <hr>
                  </center>
                </div>
                <!-- SPEEDMASTER -->
                <div class="col-12 py-3">
                  <div class="text-center">
                    <h3 class="text-uppercase">SPEEDMASTER</h3>
                  </div>
                </div>

                <!-- new row -->
                <div class="row py-5">
                  <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <p>When the Speedmaster chronograph made its debut in 1957, Omega positioned it as a watch for
                      motorsports. The Speedmaster was the world's first chronograph to have the tachymeter scale
                      engraved on the bezel rather than printed on the dial, making it easier to use with the
                      chronograph's stopwatch function to measure average speed or distances during races. The very
                      first Speedmaster was reference CK2915 with a 38mm steel case and manual-wound Caliber 321
                      movement. The first Speedy reference is easily recognizable due to its large triangular-tipped
                      "broad arrow" center hands on the black dial. In 2018, a first-generation Speedmaster CK2915 sold
                      for a record-breaking $275,508.</p>
                    <p>They replaced the CK2915 with the Speedmaster CK2998 in 1959, which included a black bezel insert
                      with the tachymeter scale instead of the engraved steel bezel and Alpha-style hands rather than
                      broad-arrow ones. Not only did this design set the blueprint of future Speedmaster chronographs,
                      but the Speedmaster CK2998 became the first Speedmaster in space when astronaut Wally Schirra wore
                      his during the Mercury-Atlas 8 mission in 1962.</p>
                    <p>This was just the beginning of the Speedmaster's involvement with space exploration. In 1965,
                      NASA qualified the Speedmaster for use during manned space missions, and in 1969, the Speedmaster
                      journeyed to the </p>
                  </div>
                  <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <p>moon with the Apollo 11 crew. While Neil Armstrong left his Speedmaster back on the Eagle Apollo
                      Lunar Module as a backup since the onboard clock malfunctioned, Buzz Aldrin wore his Speedmaster
                      Professional reference 105.012 when he took his first steps on the moon. Michael Collins, who was
                      flying the command module around the moon whilst his crewmates were on the lunar surface, wore a
                      Speedmaster Professional 145.012. Ever since Apollo 11, the Speedmaster became known as the
                      "Moonwatch."</p>
                    <p>They continued to evolve the Speedmaster line over the following decades—not just the Moonwatch
                      ones but also other Speedmaster chronographs with various sizes, designs, materials, and
                      movements. The Speedmaster Racing carries on the original intention of the Speedy as a motorsports
                      chronograph, the Speedmaster '57 are vintage-inspired pieces that take design cues from the first
                      Speedy, and the Speedmaster 38 is mainly geared towards Omega's female clientele. However, the
                      most popular Speedmaster models are still the ones that are most faithful to the early "Moonwatch"
                      - characterized as Speedmaster Professional models with 42mm steel cases, black bezels, black
                      dials, Hesalite crystals, and hand-wound mechanical movements.</p>
                  </div>
                </div>
                <!-- BROWSE POPULAR WATCHES FOR SALE -->
                <div class="col-12">
                  <div class="text-center">
                    <h3 class="text-uppercase">BROWSE POPULAR WATCHES FOR SALE</h3>
                  </div>
                  <!-- new row -->
                  <div class="row py-5">
                    <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                      <p>Bob's Watches is the leading retailer to sell or buy watches including the popular Submariner,
                        Day-Date, Datejust, and all other models. Great care is taken to price every watch at or below
                        current
                        market value as your complete satisfaction is paramount. All inventory on Bob's Watches is in
                        stock
                        and available for immediate delivery. Every watch is guaranteed to be </p>
                    </div>
                    <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                      <p>a 100% genuine, authentic and comes with a Certified warranty. Please choose from one of the
                        second-hand models for sale or visit our Sell Rolex page if you are looking to trade or sell
                        your
                        Rolex.</p>
                    </div>
                  </div>
                  <div class="row py-2">
                    <div class="col-12">
                      <center>
                        <hr>
                      </center>
                    </div>

                    <!-- SEAMASTER -->

                    <div class="col-12">
                      <div class="text-center">
                        <h3 class="text-uppercase">SEAMASTER</h3>
                      </div>
                      <!-- new row -->
                      <div class="row py-5">
                        <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                          <p>Dating back to 1948, the Seamaster is the oldest collection still in production. The
                            Seamaster lineup houses a varied assortment of timepieces from dive watches to elegant
                            sports watches to vintage-inspired watches.</p>
                          <p>Omega's diving watch range is divided into three distinct collections: the Seamaster Diver
                            300M, the Planet Ocean 600M, and the PloProf 1200M, where each model clearly states the
                            watch's water-resistance rating. Introduced in 1993 and made world-famous in 1995 as James
                            Bond’s watch in Goldeneye, the Seamaster Diver 300M is now the brands most popular diver.
                            The Seamaster Diver 300M is available in time/date, chronograph, or GMT versions, in
                            addition to plenty of material and size options.</p>
                          <p>In 2005, Omega introduced the Planet Ocean 600M collection of beefier and more
                            water-resistant dive watches. Loosely based on the 1957 Seamaster but thoroughly updated to
                            modem standards, early Planet Ocean models were offered in 42mm or 45.5mm sizes with
                            aluminum bezel inserts and helium escape valves (HEVs) for saturation diving. The following
                            year, they filled out the collection with Planet Ocean Chronograph versions and in 2011
                            Planet Ocean GMT editions were added. Although the Seamaster Planet Ocean lineup is less
                            than 15 years old, the company has frequently updated the models. Current production Planet
                            Ocean watches now feature ceramic bezels and in-house movements. In true Omega form, a
                            dizzying array of metals, sizes, colors, bracelets, and complications can be combined,
                            making the Planet Ocean a tremendously varied collection.</p>
                          <p>Characterized by its distinctive silhouette, the modern-day PloProf 1200M (a name that
                            combines the words "plongeur" and "professionnel" - French for "professional diver") is a
                            direct descendant of the Seamaster 600 Ploprof and Seamaster 1000 Ploprof from the 1970s.
                            Rather than fitting the watches with a helium escape valve, the company chose to build the
                            original Ploprof </p>
                        </div>



                        <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                          <p>cases in such a way that it wouldn't allow any helium to enter the watch in the first
                            place. While the niche diver eventually disappeared from Omega's catalog, the design was
                            revived in 2009 with the introduction of the Seamaster Ploprof 1200M—and then updated again
                            in 2016. However, it's important to note that the massive 55 x 48 mm cases of the modern
                            PloProfs are now fitted with HEVs.</p>
                          <p>Aside from divers, they also make elegant sports watches within the Seamaster Aqua Terra
                            150M collection, first introduced in 2002. As its name implies, these are watches for both
                            sea and land—designed for daily life but water-resistant to 150 meters to withstand more
                            adventurous activities. The Aqua Terra is one of the brands most diverse sub-collections,
                            available in a slew of sizes, materials, and functions. The simplest Aqua Terra watches for
                            men are the time/date models, characterized by straightforward dial layouts, a "teak" stripe
                            dial motif mimicking decks of luxury yachts, and clean round cases. Yet, the Aqua Terra
                            collection also offers a bevy of more complicated models such as day/dates, chronographs,
                            GMTs, annual calendars, and worldtimers.</p>
                          <p>Two popular vintage-inspired Seamaster models include the Seamaster 300 and the Railmaster.
                            The Seamaster 300 watches are based upon the original 1957 Seamaster 300 model—complete with
                            black timing bezels, broad arrow hands, black dials, and Arabic numerals at 3/6/9/12—but
                            slightly larger and fitted with modern movements. Additionally, along with the regular
                            production steel Seamaster 300 models, there are also precious metal versions and limited
                            editions. Similarly, the modern Railmaster watches are also based upon the original
                            Railmaster antimagnetic watch from 1957, fitted with similar dial layout designs but with
                            larger cases and newer movements.</p>
                        </div>
                      </div>
                      <div class="row py-2">
                        <div class="col-12">
                          <center>
                            <hr>
                          </center>
                        </div>
                        <!-- MOVEMENT MILESTONES -->
                        <div class="col-12 py-3">
                          <div class="text-center">
                            <h3 class="text-uppercase">MOVEMENT MILESTONES</h3>
                          </div>
                        </div>

                        <!-- new row -->
                        <div class="row py-5">
                          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <p>Since the very beginning, Omega has always focused on the performance of its calibers as
                              illustrated by the number of precision records it has achieved. The brand makes their
                              watches in Switzerland where they have always prided themselves in quality. Today, you can
                              find watches with quartz movements, hand-wound movements, or automatic movements. Plus,
                              similar to many other luxury watchmakers, the types of movements used by the company
                              evolved from in-house to modified ébauches (Lemania and ETA) and now back to a focus on
                              in-house movements.</p>
                            <h4>CALIBER 321</h4>
                            <p>In the 1930s, Omega, Tissot, and Lemania (a Swiss watch movement manufacturer) formed a
                              joint venture called SSIH (Société Suisse pour l'Industrie Horlogère). Therefore, Lemania
                              produced several movements for Omega but none as famous as the Caliber 321, which served
                              to power early Speedmaster chronographs from the 1950s and 1960s. The manual-wound Caliber
                              321 lateral clutch chronograph movement is noted for its beautiful design but they
                              eventually replaced it with the less costly and easier to produce Lemania-based Caliber
                              861 in 1969. However, in 2019, the brand announced the return of the Caliber 321.</p>
                          </div>
                          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">

                            <h4>CO-AXIAL ESCAPAMENT</h4>
                            <p>In 1999, Omega introduced its first movement fitted with a Co-Axial escapement. Invented
                              by George Daniels, the greatest benefit of the Co-Axial escapement is that it produces
                              less friction than traditional lever escapements. Less friction means less lubrication is
                              needed in the movement, which translates to improved reliability and longer intervals
                              between servicing.</p>
                            <h4>MASTER CHRONOMETER CERTIFICATION</h4>
                            <p>In 2015, they introduced the Master Chronometer Certification, which denotes that along
                              with a COSC (Official Swiss Chronometer Testing Institute) certification, a movement has
                              also passed a series of eight tests set out by METAS (The Federal Institute of Metrology).
                              Master Chronometer watches have a minimum water-resistance rating of 100 meters, a minimum
                              power reserve rating of 60 hours, an accuracy rating of 0/+5 seconds per day, and are
                              resistant to magnetic fields of 15,000 gauss. The Master Chronometer Certification debuted
                              on the Globemaster but they now offer it across many more of its watch collections.</p>
                          </div>
                        </div>
                        <div class="row py-2">
                          <div class="col-12">
                            <center>
                              <hr>
                            </center>
                          </div>

                          <!-- difference between certified -->
                          <div class="col-12 py-3">
                            <div class="text-center">
                              <h3 class="text-uppercase">PARTNERSHIPS AND BRAND AMBASSADORS</h3>
                            </div>
                          </div>
                          <!-- new row -->
                          <div class="row py-5">
                            <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                              <p>Among all the partnerships, some of the most famous are its relationships with NASA,
                                the Olympic Games, golf, swimming, sailing, ocean exploration/conservation, and cinema.
                                As such, they count several famous personalities as brand ambassadors including (but
                                certainly not limited to the following list of noteworthy individuals. </p>

                            </div>
                            <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                              <p>George Clooney Nicole Kidman Cindy Crawford Eddie Redmayne Daniel Craig Kaia Gerber
                                Rory McIlroy Sergio Garcia Michael Phelps Buzz Aldrin</p>
                              <p>Omega has also been the official watch of James Bond since 2005. In addition to watches
                                appearing in the films, the company releases special edition James Bond Seamaster
                                watches with every 007 movie release.</p>

                            </div>
                          </div>
                          <div class="row py-2">
                            <div class="col-12">
                              <center>
                                <hr>
                              </center>
                            </div>
                            <!-- new iframe -->
                            <div class="col-12 py-3">
                              <div class="text-center">
                                <h3 class="text-uppercase">OMEGA VS ROLEX</h3>
                              </div>
                            </div>
                            <!-- new row -->
                            <div class="row py-5">
                              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <p>When it comes to accuracy, the Omega quartz watches are more accurate than mechanical
                                  alternatives. You won't have this option with Rolex since the </p>

                              </div>
                              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <p>brand no longer produces quartz watches anymore.</p>

                              </div>
                            </div>
                            <div class="row py-2">
                              <div class="col-12">
                                <center>
                                  <hr>
                                </center>
                              </div>

                              <div class="col-12 py-3">
                                <div class="text-center">
                                  <h3 class="text-uppercase">BUYING OMEGA WATCHES</h3>
                                </div>
                              </div>
                              <!-- new row -->
                              <div class="row py-5">
                                <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                  <p>When searching for the perfect luxury watch it is difficult to beat the reputation
                                    that OMEGA watch has built. The brand has produced stunning vintage models and
                                    continues to manufacturer numerous examples of innovative modern timepieces. Bob's
                                    Watches carries a number of </p>

                                </div>
                                <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                  <p>timepieces at some of the lowest prices on the market. OMEGA's advanced Swiss
                                    craftsmanship has earned them a place among the most prestigious brands.</p>

                                </div>
                              </div>
                            </div>
                          </div>
  </section>

  <?php include('footer.php'); ?>