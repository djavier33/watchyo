<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    <meta name="author" content="INSPIRO" />    
	<meta name="description" content="Themeforest Template Polo, html template">
    <link rel="icon" type="image/png" href="images/favicon.png">   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">
    <!-- Document title -->
    <title>POLO | The Multi-Purpose HTML5 Template</title>
    <!-- Stylesheets & Fonts -->
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="custom.css">
    <link href="css/custom.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> -->
    <!-- Slick Stylesheet --> 
    <link rel="stylesheet" type="text/css" href="js/vendor/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="js/vendor/slick/slick-theme.css"/>
    <!-- Font webfont --->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

</head>

<body>


<!-- Body Inner -->
<div class="body-inner">

     <!-- Topbar -->
     <div id="topbar" class="d-none d-xl-block d-lg-block topbar-fullwidth">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="top-menu">
 
                            <li><a href="#"><i class="far fa-envelope"></i> info@watchyo.com</a></li>
                            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> CALL: 800.494.3708</a></li>
                            <li><a href="#"><i class="far fa-comment-dots"></i>TEXT: 714.500.7958</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 d-none d-sm-block">

                            <div class="social-icons social-icons-colored-hover">
                            <ul>
                                
                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-google"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                              
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Topbar -->

        <!-- Header -->
        <header id="header" data-fullwidth="true">
            <div class="header-inner">
                <div class="container">
                    <!--Logo-->
                    <div id="logo"> <a href="index.html"><span class="logo-default"><img src="images/logo-img.svg"/></span><span class="logo-dark"><img src="images/logo-img.svg"/></span></a> </div>
                    <!--End: Logo-->
                    <!-- Search -->
                    <div id="search"><a id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i class="icon-x"></i></a>
                        <form class="search-form" action="search-results-page.html" method="get">
                            <input class="form-control" name="q" type="text" placeholder="Type & Search..." />
                            <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                        </form>
                    </div> <!-- end: search -->
                    <!--Header Extras-->
                    <div class="header-extras">
                        <ul>
                            <li> <a id="btn-search" href="#"> <i class="icon-search"></i></a> </li>
                            <li>
                                <div class="p-dropdown"> <a href="#"><i class="icon-globe"></i><span>EN</span></a>
                                    <ul class="p-dropdown-content">
                                        <li><a href="#">French</a></li>
                                        <li><a href="#">Spanish</a></li>
                                        <li><a href="#">English</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--end: Header Extras-->
                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger"> <a class="lines-button x"><span class="lines"></span></a> </div>
                    <!--end: Navigation Resposnive Trigger-->
                    <!--Navigation-->
                    <div id="mainMenu">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li class="dropdown"><a href="#">Rolex</a>
                                      
                                    </li>
                                    <li class="dropdown"><a href="#">Omega</a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-submenu"><a href="#">Test</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="widgets.html">Luxury Watchs</a></li>
                                            <li><a href="page-loaders.html">New Arrivals</a></li>
                                            <li class="dropdown-submenu"><a href="#">Modal Auto Open<span class="badge badge-danger">NEW</span></a>
                                            <ul class="dropdown-menu">
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                    <li><a href="#">Test</a></li>
                                                </ul>s
                                            </li>
                                            <li class="dropdown-submenu"><a href="#">Vintage Rolex<span class="badge badge-danger">NEW</span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="cookie-notify.html">Top position</a></li>
                                                    <li><a href="cookie-notify-bottom.html">Bottom position</a></li>
                                                </ul>
                                            </li>
                                            
                                            <li><a href="shape-dividers.html">Shape Dividers<span class="badge badge-danger">NEW</span></a></li>
                                            <li class="dropdown-submenu"><a href="#">Menu Labels</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Label (new)<span class="badge badge-danger">NEW</span></a></li>
                                                    <li><a href="#">Label (hot)<span class="badge badge-danger">HOT</span></a></li>
                                                    <li><a href="#">Label (popular)<span class="badge badge-success">POPULAR</span></a></li>
                                                    <li><a href="#">Label (sale)<span class="badge badge-warning">SALE</span></a></li>
                                                    <li><a href="#">Label (info)<span class="badge badge-info">INFO</span></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="cart.php">Cart</a>
                                      
                                    </li>
                                    <li class="dropdown"><a href="checkout.php">Checkout</a>
                                      
                                    </li>
                                    
                                    <li class="dropdown"><a href="rolex.php">RolexDemo</a>
                                      
                                    </li>
                                    <li class="dropdown"><a href="omega.php">OmegaDemo</a>
                                      
                                      </li>
                              
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->
       
      <!--end: Inspiro Slider -->