<?php include('header-page.php'); ?>
<div class="body">

<section class="your-cart py-0">
    <div class="container">
    <div class="cart-heading">
            <a href=""> <i class="fa fa-caret-left"></i></a>
            <h1>YOUR CART</h1>
    </div>
    </div>
</section>

<section class="product-table">
    <div class="container">
   <div class="row">
       <table> 
        <!-- <form action="#cart" method="post">
             -->
             <tbody>
                <tr class="shopping-cart-item-header py-3">
                    <th class="shopping-cart-info"><strong>Product Information</strong></th>
                    <th class="shopping-cart-qty"><strong>Qty</strong></th>
                    <th class="shopping-cart-each"><strong>Each</strong></th>
                    <th class="shopping-cart-total"><strong>Total</strong></th>
                    <th class="shopping-cart-button"></th>
                </tr>

                <tr class="shopping-cart-row">
                <td class="shopping-cart-items-info">    
                        <div class="cart-image">
                            <img src="images/rolex-2.jpg" alt="">
                        </div>
                        <div class="info">
                            <span>Omega Bullhead</span>
                            <p>sku: <strong>133869 X</strong></p>
                        </div>
                    </td>
                
                    <td class="shopping-cart-items-qty">
                        <span><strong>$7,895.00</strong></span>
                    </td>
                                    
                    <td class="shopping-cart-items-each">
                        <div class="each-cell">
                            <div class="cell-1">
                                <i class="fa fa-times"></i>
                                <span><a href="">Remove</a> </span>
                            </div>
                            <div class="cell-2">
                                <span>  Quantity:</span>
                                <input type="text" value="1">
                            </div>
                        </div>
                        <div class="add-certification-btn">
                                <input type="button" value="ADD CERTIFICATION">
                        </div>
                    </td>

                    <td class="shopping-cart-items-total">
                       <div class="subtotal-cell">
                           <div class="cell-1">
                                <span>Subtotal:</span>
                           </div>
                           <div class="cell-2">
                                <span><strong>$7,895.00</strong></span>
                           </div>
                       </div>
                    </td>
                    
                    <td class="shopping-cart-items-button">
                        <input type="button" value="UPDATE">
                    </td>
                </tr>
                </tbody>
         
                

        <!-- </form> -->
    </table>
       
   </div>
</div>
</section>

<section class="py-0">
    <div class="container">
        <div class="row">
            <div class="col-6 right">
                    <h1 class="checkout-title text-uppercase py-0">Checkout</h1>
            <hr>
                <div class="checkout-buttons">
                    <div class="paypal-btn">
                    <a href="">Checkout with </a><i class="fab fa-paypal"></i>
                    </div>
                    <div class="amazon-btn">
                    <i class="fab fa-amazon"></i>
                    <a href="">Checkout with </a>
                    </div>
                    <div class="checkout-or">
                        <span class="or">OR</span>
                    </div>
                    
                </div>
            </div>
            
            <div class="col-6 left">

            <form action="POST" action="verify_checkout">

                <div class="account-login">
                    Have an accout? 
                    <a href="#sign-in-for-faster-checkout">Sign in for faster checkout</a>
                </div>
                <div class="payment-heading">
                    <div class="subtotal">
                        <h4><strong>Subtotal:</strong>$7,895.00</h4>
                    </div>
                    <h2 class="text-uppercase">Payment Method</h2>
                    <p>Save <a href="#discount">3% off</a> by choosing wire</p>
                </div>
                <div class="select-payment-method">

                <div class="subtotal-btn">
                    <input type="button" value="PROCEEED TO CHECKOUT">
                </div>
                <div class="subtotal-method">
                    <span><strong>We accept:</strong></span>
                    <img src="images/card-images.png" alt="">
                </div>

                    <select name="select-payment-method" id="select-payment-method">
                        <option value="Please-select-payment-method">Please select payment method</option>
                        <option value="wire-transfer">Wire Transfer</option>
                        <option value="credit-card">Credit Card</option>
                        <option value="paypal">PayPal</option>
                    </select>
                </div>
                <div class="verify">
                    <a href="">Verify Checkout</a>
                </div>
                
                </form>
            </div>
        </div>
   
        
    </div>
</section>
<?php include('footer.php'); ?>