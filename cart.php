<?php include('header-page.php'); ?>
<div class="body">


<section>
    <div class="container">
    <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-12">
                <div class="product-wrapper">
                    <div class="product-image">  
                        <img class="img-fluid" src="images/rolex-2.jpg" alt="product-1">
                        <br>
                        <span class="text-uppercase"> Actual Photo</span>
                    </div>
                    <div class="tiny-images">
                    <div class="product-additional-image">
                        <a href="">
                            <img src="images/rolex-tiny-image1.jpeg" alt="">
                        </a>
                        <a href="">
                            <img src="images/rolex-tiny-image3.jpeg" alt="">
                        </a>
                        <a href="">
                            <img src="images/rolex-tiny-image3.jpeg" alt="">
                        </a>
                        <a href="">
                            <img src="images/rolex-tiny-image3.jpeg" alt="">
                        </a>
                    </div>
            </div>  
            </div>   
            </div>
            
                   
           

            <div class="col-xl-6 col-lg-6 col-md-6 col-12 table-detail">
                
                            <div class="product-headline">
                                <h4 class="text-uppercase"> rolex datejust 41 126334 blue dial</h4>
                            </div>

                            <div class="people-viewing">
                                <strong>4</strong> PEOPLE VIEWING
                            </div>
                            <hr>
                            <!-- buy Button -->
                            <div class="price">
                                <h3 class="my-0">$7,895</h3>
                                <span>Cash wire Price</span>
                            </div>

                            <div class="pricecontainer">
                            <form action="">
                                    <input type="button" value="BUY IN NOW">
                                </form>
                            </div>

                            <div class="product-certified-authentic">
                                <p>
                                    <strong>100%</strong> CERTIFIED AUTHENTIC <i class="fa fa-info-circle px-2"></i>
                                </p> 
                            </div>
                           
                            
                            <!-- product saleswatch -->
             <img class="image-light-box" src="images/certified-authentic.jpeg" alt="">                       
            <div class="product-saleswatch">
                    <p>Looking to sell? <a href="sell-your-rolex"> Sell Your Rolex</a></p>
            </div>
          

            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="row"><strong>Brand:</strong></th>
                        <td colspan="2">Rolex</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Regular Price:</strong></th>
                        <td colspan="2">	$12,857.36</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Item Number:</strong></th>
                        <td colspan="2">	141370 x</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Condition:</strong> 
                        <a href="">(What's This?)</a></th>
                        <td colspan="2">	Excellent</td>
                    </tr>
                    
                    <tr>
                        <th scope="row"><strong>Model Name/Number:</strong></th>
                        <td colspan="2">Datejust 41 - 126334</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Serial/Year:</strong></th>
                        <td colspan="2">	Random - 2020</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Gender:</strong></th>
                        <td colspan="2">Men's</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Movement:</strong></th>
                        <td colspan="2">Automatic 3235</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Case:</strong></th>
                        <td colspan="2">	Stainless steel (41mm) w/ 18k white gold Fluted bezel, scratch resistant sapphire crystal and inner reflector ring engraved with serial number</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Dial:</strong></th>
                        <td colspan="2">		Blue w/ Roman numeral hour markers </td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Bracelet:</strong></th>
                        <td colspan="2">	Stainless steel Jubilee w/ Oysterclasp </td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Box & Papers:</strong></th>
                        <td colspan="2">	Rolex box and warranty card dated 12/2020</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Warranty:</strong></th>
                        <td colspan="2">	This pre-owned Rolex comes with Bob's Watches one year warranty.</td>
                    </tr>
                    <tr>
                        <th scope="row"><strong>Return Policy:</strong></th>
                        <td colspan="2">	If not 100% satisfied, return watch in 3 days for a full refund. <a href="">Terms</a> .</td>
                    </tr>
                </tbody>
            </table>
            
            <div class="pre-own">
           
                <h2 class="text-uppercase"> pre-owned watch condition examination</h2>
                <p>This Rolex watch is in overall excellent unpolished condition with faint wear to the bracelet, case and bezel. One factory sticker intact on inside of clasp.</p>
                <h2 class="text-uppercase"> UNPOLISHED ROLEX DATEJUST 41 REF 126334</h2>
                    <p class="text-left">  <i>By</i> Paul Altieri </p>
                    <div class="col-12 image-expand">
                        <img class="text-center" src="images/certified-authentic.jpeg" alt="">
                    <span>click to Expand</span>
                    </div>  
                <div class="col-12 pre-owned"> 
                    <p><b>*All Pre-Owned Rolex watches are guaranteed to be 100% genuine and certified authentic.</b> 
                        All watches are shipped to fit the average size wrist (7 1/4" - 7 1/2" for men and 6 1/4" - 6 1/2" for women). Additional links can be purchased if needed.</p>
                    <p class="text-center"> <b>HAVE A QUESTION? 800.494.3708  |  MON - FRI 9AM - 5PM (PST)</b> </p>
                </div>
                </div>
            </div>
    </div>

</div>
    </div>
</div>
</section>


<section class="review pt-0">
    <div class="container">
        <div class="row">
            <!-- product image --> 
            <!-- Breadcrumb -->
            <div class="col-12">
                <div class="row">
                    <div class="col-12 breadcrumb">
                        <ul>
                            <li>
                                <i class="fa fa-home"></i>
                            </li>
                            <li>
                                <a href="">Rolex Watches</a>
                            </li>
                            <li>
                                <a href="">DateJust</a>
                            </li>
                            <li>
                                <a href=""> Model Ref.No 126334</a>
                            </li>
                            <li>
                            <a href="">Rolex Datejust 41 126334 Blue Dial</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Customer Also Viewed -->
            <div class="col-12">
                <div class="customer-also-viewed">
                    <h2 class="text-center">Customer Also Viewed</h2>
                    <hr>
                </div>
            </div>
            <div class="col-12 py-3">
                <div class="slider">
                <ul>
                    <li>
                        <img src="images/rolex-2" alt="" class="img-fluid w-50">
                        <h4>mens rolex datejust blue 126334</h4>
                        <p>Blue Dial, 18k White Gold Bezel Stainless Steel Oyster</p>
                        <h3>$11,795.00</h3>
                    </li>
                    <li class="text-center">
                        <img src="images/rolex-2" alt="" class="img-fluid w-50">
                        <h4>mens rolex datejust blue 126334</h4>
                        <p>Blue Dial, 18k White Gold Bezel Stainless Steel Oyster</p>
                        <h3>$11,795.00</h3>
                    </li>
                    <li class="text-center">
                        <img src="images/rolex-2" alt="" class="img-fluid w-50">
                        <h4>mens rolex datejust blue 126334</h4>
                        <p>Blue Dial, 18k White Gold Bezel Stainless Steel Oyster</p>
                        <h3>$11,795.00</h3>
                    </li>
                    <li class="text-center">
                        <img src="images/rolex-2" alt="" class="img-fluid w-50">
                        <h4>mens rolex datejust blue 126334</h4>
                        <p>Blue Dial, 18k White Gold Bezel Stainless Steel Oyster</p>
                        <h3>$11,795.00</h3>
                    </li>
                </ul>
                </div>
            </div>
        </div>

           
        </div>
    </div>
</section>


<section class="py-0">
    <div class="container">
        <div class="row">
             <!-- Product Reviews -->
                <div class="col-12 py-3">
                <div class="product-reviews text-uppercase pt-3">
                        <h2 class="product-reviews-title"><span>Product Reviews</span></h2>
                        </div>
                </div>

            
                <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 review-review">
                        <div class="product-review-left">
                            <span> <strong>Rolex Datejust 41 126334 Blue Dial</strong> </span>
                         </div>
                        <div class="total-average">
                            <span class="average-star">
                                <i class="fa fa-star" arial-hidden="true"></i>
                                <i class="fa fa-star" arial-hidden="true"></i>
                                <i class="fa fa-star" arial-hidden="true"></i>
                                <i class="fa fa-star" arial-hidden="true"></i>
                                <i class="fa fa-star" arial-hidden="true"></i>
                                <strong>5 </strong>
                            </span>
                            <br>
                        </div>
                  
                <!-- progress bar 1 -->
                <div class="rate">      
                            <div class="rate-star">
                                    <span>0 star</span>
                            </div>
                            <div class="rate-bar">
                                <div class="progress">
                                        <div class="progress-bar bg-warning px-0" 
                                        role="progressbar" 
                                        style="width: 100%"
                                        aria-valuenow="100" 
                                        aria-valuemin="0" 
                                        aria-valuemax="100"></div>
                                    </div>
                            </div>
                            <div class="rate-percent">
                                    <span>100%</span>
                            </div>
                    </div>
                    <div class="rate">      
                            <div class="rate-star">
                                    <span>0 star</span>
                            </div>
                            <div class="rate-bar">
                                <div class="progress">
                                        <div class="progress-bar bg-warning px-0" 
                                        role="progressbar" 
                                        style="width: 0%"
                                        aria-valuenow="0" 
                                        aria-valuemin="0" 
                                        aria-valuemax="0"></div>
                                    </div>
                            </div>
                            <div class="rate-percent">
                                    <span>0%</span>
                            </div>
                    </div>
                    <div class="rate">      
                            <div class="rate-star">
                                    <span>0 star</span>
                            </div>
                            <div class="rate-bar">
                                <div class="progress">
                                        <div class="progress-bar bg-warning px-0" 
                                        role="progressbar" 
                                        style="width: 0%"
                                        aria-valuenow="0" 
                                        aria-valuemin="0" 
                                        aria-valuemax="100"></div>
                                    </div>
                            </div>
                            <div class="rate-percent">
                                    <span>0%</span>
                            </div>
                    </div>
                    <div class="rate">      
                            <div class="rate-star">
                                    <span>0 star</span>
                            </div>
                            <div class="rate-bar">
                                <div class="progress">
                                        <div class="progress-bar bg-warning px-0" 
                                        role="progressbar" 
                                        style="width: 0%"
                                        aria-valuenow="0" 
                                        aria-valuemin="0" 
                                        aria-valuemax="100"></div>
                                    </div>
                            </div>
                            <div class="rate-percent">
                                    <span>0%</span>
                            </div>
                    </div>
                    <div class="rate">      
                            <div class="rate-star">
                                    <span>0 star</span>
                            </div>
                            <div class="rate-bar">
                                <div class="progress">
                                        <div class="progress-bar bg-warning px-0" 
                                        role="progressbar" 
                                        style="width: 0%"
                                        aria-valuenow="0" 
                                        aria-valuemin="0" 
                                        aria-valuemax="100"></div>
                                    </div>
                            </div>
                            <div class="rate-percent">
                                    <span>0%</span>
                            </div>
                    </div>
                    

                    
                    
                        <div class="write-review-btn first-btn text-center py-2">
                            <input type="button" value="Write a review" class="text-uppercase">
                        </div>         
                    
                </div>
                
            <!-- Write a review -->
        



            <!-- Reviews -->
            <div class="col-xxl-6 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="customer-review">
                    <span class="average-star">
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                    </span>
                    <h3 class="customer-name my-0">Mark</h3>
                    <span class="customer-reviewed-date">March 18, 2021</span>
                    <h3 class="product-title my-0">Datejust 41</h3>
                    <p class="customer-comment">Easy process and easy to communicate and deal with. 
                        Watch was perfect as described and legit.
                    </p>
                </div>
            </div>
            <div class="col-xxl-6 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="customer-review">
                    <span class="average-star">
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                        <i class="fa fa-star" arial-hidden="true"></i>
                    </span>
                    <h3 class="customer-name my-0">Mark</h3>
                    <span class="customer-reviewed-date">March 18, 2021</span>
                    <h3 class="product-title my-0">Datejust 41</h3>
                    <p class="customer-comment">Easy process and easy to communicate and deal with. 
                        Watch was perfect as described and legit.
                    </p>
                </div>
            </div>
           
        
            <!-- view more reviews -->
            <div class="col-12">
                <div class="write-review-btn d-flex align-items-center justify-content-center pt-5">
                        <input type="button" value="View More Review" class="text-uppercase">
                </div>  
            </div>
              
        </div>
    </div>
</section>



<section>
      <div class="container">
          <div class="row">
              <!-- testimonials -->
      <div class="col-12">
              <div class="testimonials text-uppercase py-2">
                    <h2 class="testimonial-title"><span>Testimonials</span></h2>
              </div>
          </div>
          <div class="col-12 d-flex flex-column align-items-center justify-content-center text-center py-2">
              <img class="image-fluid w-50" src="images/rolex-tiny-image1.jpeg" alt="">
              <span class="average-star">
                <i class="fa fa-star" arial-hidden="true"></i>
                <i class="fa fa-star" arial-hidden="true"></i>
                <i class="fa fa-star" arial-hidden="true"></i>
                <i class="fa fa-star" arial-hidden="true"></i>
                <i class="fa fa-star" arial-hidden="true"></i>
                <span><strong>4.8</strong></span>
            </span>
            <label>2898 verified reviews in the past 12months</label>
            <span>
                <i class="fa fa-star"></i>
                <span>Google Customer Reviews</span>
            </span>
          </div>

          <!-- Google Customer Reviews -->
          <div class="col-12">
          <div class="google-customer-review pt-5">
              <div class="rate">
                <span class="average-star">
                    <i class="fa fa-star" arial-hidden="true"></i>
                    <i class="fa fa-star" arial-hidden="true"></i>
                    <i class="fa fa-star" arial-hidden="true"></i>
                    <i class="fa fa-star" arial-hidden="true"></i>
                    <i class="fa fa-star" arial-hidden="true"></i>
                    <span><strong>5/5</strong></span>
                </span>
              </div>
              <div class="google-customer-comment">
                  <p>Watch was exactly as advertised and in execellent condition.
                      Thank you for providing top notch services!
                  </p>
              </div>
          </div>
          </div>
          </div>
      </div>
</section>



<?php include('footer.php'); ?>