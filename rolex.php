<?php include('header-page.php'); ?>
<div class="body">

  <!-- Select Model -->
  <section class="tag-section">
    <div class="container">
      <div class="select-model">
        <div class="text-center text-uppercase">
          <h3>
            shop used rolex watches for sale
          </h3>
        </div>
        <!-- pagination -->
        <div class="pt-3">
          <!-- this the pagination buttons -->
          <div class="tags">
            <ul class="text-uppercase  d-flex flex-row list-unstyled">
              <li><a href="#submariner">submariner</a></li>
              <li><a href="#rolex-16610">rolex 16610</a></li>
              <li><a href="#datejust">datejust</a></li>
              <li><a href="#daytona">daytona</a></li>
              <li><a href="#day-date">day-date</a></li>
              <li><a href="#president">president</a></li>
              <li><a href="#gmt-master">gmt-master</a></li>
              <li><a href="#explorer">explorer</a></li>
              <li><a href="#yachtmaster">yachtmaster</a></li>
              <li><a href="#air-king">air-king</a></li>
              <li><a href="#milgauss">milgauss</a></li>
              <li><a href="#rolex-116500">rolex-116500</a></li>
              <li><a href="#datejust-||">datajust ||</a></li>
              <li><a href="#rolex-116500">rolex-116500</a></li>
              <li><a href="#rolex-116500">rolex-116500</a></li>
              <li><a href="#rolex-116500">rolex-116500</a></li>
              <li><a href="#rolex-116500">rolex-116500</a></li>
            </ul>
          </div>

        </div>

        <!-- product found -->

        <div
          class="row d-flex justify-content-xxl-between justify-content-xl-between justify-content-xl-between align-items-center">
          <div class="col-xxl-3 col-xl-3 col-md-6 col-sm-6 col-12 text-center">
            <strong><span>481 </span>product found</strong>
          </div>
          <div class="col-xxl-3 col-xl-3 col-md-6- col-sm-6 col-12 text-center text-center">
            <h4><a href="#sellMyRolex">Sell my rolex</a></h4>
          </div>
        </div>
        <hr>
      </div>

  </section>

  <!-- product and filter nav-->
  <section class="p-0">

    <!-- Side Bar -->
    <div class="container">
      <div class="side-bar" id="side-bar">
        <div class="compare-watches mb-2">
          <a class="side" href="#"> Compare Watches</a>
        </div>
        <!--new/used-->

        <div class="filter-nav py-2">
          <h6 class="text-uppercase">New/Used</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
              Used
            </label>
            <span>(474)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
              New
            </label>
            <span>(213)</span>
          </div>
        </div>
        <!--end-new-/used-->

        <!--Size-->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Size</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">44mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">42mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">41mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">39mm</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">38mm</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#size">+View More</a>

          <div id="size" class="collapse">

            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">37mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">36mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">34mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">31mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">29mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">28mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">26mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">24mm</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">15mm</label><span>(0)</span>
            </div>
          </div>
        </div>
        <!-- end-size  -->
        <!-- metal-type -->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Metal Type</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Stainless Steel</label><span>(298)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Steel and Gold</label><span>(111)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Yellow Gold</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">White Gold</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Rose Gold</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#metal-type">+View More</a>

          <div id="metal-type" class="collapse">

            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Steel and Rose Gold</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Titanium</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Platinum</label><span>(0)</span>
            </div>

          </div>
        </div>
        <!--end-metal-type-->
        <!-- price -->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Price</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Under $2,000</label><span>(298)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$2,000 - 5,000</label><span>(111)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$5,000 - 7,000</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$7,000 - 10,000</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">$10,000 - 15,000</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#price">+View More</a>

          <div id="price" class="collapse">

            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">$15,000 - 20,000</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Over $20,000</label><span>(0)</span>
            </div>
          </div>
        </div>
        <!-- end-price -->
        <!-- Color -->
        <div class="filter-nav py-2">
          <h6 class="text-uppercase">Color</h6>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input form-check-inline black" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Black</label><span>(298)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Champagne</label><span>(111)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Silver</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">White</label><span>(0)</span>
          </div>
          <div class="form-check d-flex justify-content-between">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">Blue</label><span>(0)</span>
          </div>
          <!-- DropDown -->

          <a href="" data-toggle="collapse" data-target="#color">+View More</a>

          <div id="color" class="collapse">
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Diamonds</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">State</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Green</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Brown</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Ivory</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Rose</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Red</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Mother of Pearl</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Purple</label><span>(0)</span>
            </div>
            <div class="form-check d-flex justify-content-between">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">Wood</label><span>(0)</span>
            </div>
          </div>
        </div>
      </div>

      <div class="more-filter">
        <div class="container">
          <div class="row">
            <div class="col">
              <input type="button" class="more-filter-btn" id="moreFilter" value="More Filter">
            </div>
          </div>
        </div>
      </div>
      <!-- prodcut list -->
      <div class="productlist">
        <div class="container">
          <div class="row product-main justify-content-around align-items-center">
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-2.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-3.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-4.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-5.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-1.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-2.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-3.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/shop-watch-4.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-6 col-sm-8 col-12 p-2 item">
              <div class="product-img text-center">
                <img src="images/rolex-2.jpg" class="img-fluid w-50" alt="">
                <h2><a href="#">Rolex Daytona Ice Blue</a></h2>
                <p>Ice Blue Dial, Brown Ceramic Bezel Platinum OysterBand, B&P(2021)</p>
              </div>
              <div class="d-flex justify-content-center align-items-center">
                <div class="product-btn-buy text-center d-flex flex-column">
                  <label for="">$120,995</label>
                  <input type="button" value="Buy">
                </div>
                <div class="product-btn-sell text-center d-flex flex-column">
                  <label for="">Get Quote</label>
                  <input type="button" value="Sell">
                </div>
              </div>
            </div>

            <!-- End of Product List -->
            <!-- pagination -->
            <div class="col-12 pt-5">
              <div class="d-flex justify-content-center align-items-center">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
            <!-- end of pagination -->
          </div>
        </div>
  </section>

  <div class="container">
    <div class="embed-responsive embed-responsive-4by3">
      <iframe class="embed-responsive-item rolex-video" src="https://www.youtube.com/embed/ASAe3UQUBoc?rel=0"
        allowfullscreen></iframe>
    </div>
  </div>

  <section class="text-content">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h3>New Rolex Model</h3>
          <p>The global pandemic resulted in the delay of many new watch releases, and for several months, it looked
            like there might not be any new Rolex watches in 2020 at all. However, on September 1st, Rolex unveiled its
            new 2020 watch collections, making a number of key updates to the brand's iconic catalog. The four Rolex
            collections that saw new watches join them for 2020 are the Submariner, Oyster Perpetual, Sky-Dweller and
            Datejust.</p>
        </div>
        <div class="col-12">
          <h3>ROLEX SUBMARINER</h3>
          <p>The 2020 updates to the Rolex Submariner mark the introduction of an all-new generation of Rolex’s
            legendary dive watch. All of the previous 40mm models have been discontinued and replaced by new models with
            41mm cases. Following the 2020 update, the Rolex Submariner collection now consists of the following
            references.</p>
          <ul class="px-5">
            <li>Ref. 124060: Oystersteel, Black Cerachrom Bezel, Black Dial (No-Date)</li>
            <li>Ref. 126610LN: Oystersteel, Black Cerachrom Bezel, Black Dial</li>
            <li>Ref. 126610LV: Oystersteel, Green Cerachrom Bezel, Black Dial</li>
            <li>Ref. 126613LN: Yellow Rolesor; Black Ceramic Bezel, Black Dial</li>
            <li>Ref. 126613LB: Yellow Rolesor; Blue Ceramic Bezel, Blue Dial</li>
            <li>Ref. 126618LN: Yellow Gold, Black Cerachrom Bezel, Black Dial</li>
            <li>Ref. 126618LB: Yellow Gold, Blue Cerachrom Bezel, Blue Dial</li>
            <li>Ref. 126619LB: White Gold, Blue Cerachrom Bezel, Black Dial</li>
          </ul>
          <p>Official retail prices for the new 41mm generation of Submariner watches start out at $8,100 for the
            standard, stainless steel no-date model and $9,150 for the version with the date complication. From there,
            prices increase with precious metals, topping out at $39,650 for the solid 18k white gold model fitted with
            a black dial and a blue Cerachom bezel insert.</p>
          <p>With that in mind, due to demand far exceeding supply at a retail level, the second-hand prices for the new
            2020 Submariner models are expected to be quite a bit higher than their original retail prices. It is still
            too early to tell what the overall availability of these models will look like going forward, but some of
            the very first examples of the stainless steel models to surface on the secondary market are currently
            trading hands for roughly twice their original retail prices.</p>
        </div>
        <div class="col-12">
          <h3 class="text-uppercase"> rolex sky-dweller</h3>
          <p>Next to the Submariner, the Rolex Oyster Perpetual was the collection that saw the next largest update for
            2020. The biggest update to the Oyster Perpetual was the introduction of a new 41mm model, replacing the
            fan-favorite 39mm version within the Oyster Perpetual lineup. Additionally, the smallest size option
            increased from 26mm to 28mm, and the entire collection received new movements, bringing the Oyster Perpetual
            into the modern era. The new 2020 Rolex Oyster Perpetual collection</p>
          <ul class="px-5">
            <li>Ref. 276200: 28mm case diameter</li>
            <li>Ref. 277200: 31mm case diameter</li>
            <li>Ref. 124200: 34mm case diameter</li>
            <li>Ref. 126000: 36mm case diameter</li>
            <li>Ref. 124300: 41mm case diameter</li>
          </ul>
          <p>Official retail prices are $40,000 for the yellow gold model and $41,500 for the Everose gold version,
            regardless of the dial color fitted to these two watches. As the Oysterflex bracelet remains a highly
            desirable option within Rolex’s catalog, exclusively fitted to the brand’s solid 18k gold models, many
            expect these new 2020 Oysterflex Sky-Dweller models to retail their value quite well, and some even predict
            that they may end up trading hands for slight premiums above their original retail prices.</p>
        </div>
        <div class="col-12">
          <h3 class="text-uppercase">datejust 31</h3>
          <p>Rolex has continuously been updating its iconic Datejust collection over the years, and although the 36mm
            and 41mm were just updated in previous years, 2020 marked the arrival of new Datejust 31 watches in
            stainless steel and White Rolesor.</p>
        </div>
        <ul class="px-5">
          <li>Ref. 278240: Stainless steel, smooth bezel</li>
          <li>Ref. 278274: White Rolesor, fluted bezel</li>
          <li>Ref. 278344RBR: White Rolesor, diamond-set bezel</li>
          <li>Ref. 278384RBR: White Rolesor, diamond-set bezel</li>
        </ul>
        <p>All of the models are powered by Rolex’s Caliber 2236 movement, and feature stainless steel cases paired with
          either steel or 18k white gold bezels. Retail prices range from $6,600 for the entirely stainless steel model
          up to $16,050 for the model with the full-diamond bezel. As these new Datejust 31 models are not sports
          watches, it is expected that their second-hand prices will stay more-or-less close to their original retail
          values, and may anticipate that the diamond-set models will ultimately be able to be found at a discount.</p>
      </div>
      <!-- About Rolex -->
      <div class="col-12">
        <div class="text-center">
          <h3 class="text-uppercase">about rolex watches</h3>
        </div>
        <!-- new row -->
        <div class="row py-5">
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <p>Founded by Hans Wilsdorf in 1905, Rolex has grown significantly in both size and prestige over the course
              of its long and illustrious history and has carefully cultivated a reputation that is synonymous with
              quality, performance, and exclusivity. Today, the company is the world's leading luxury timepiece
              manufacturer, crafting nearly a million watches per year, and consistently earning a spot on Forbes' list
              of the World's Most Powerful Brands.</p>
            <p>The company has been at the forefront of mechanical watchmaking and has become one of the most well-known
              and respected luxury brands in the entire world. Synonymous with precision, quality, and exclusivity, they
              are instantly recognizable for their iconic designs, making them an internationally recognized symbol of
              success and personal accomplishment. The selection of Rolex watches for men and women is impressive
              offering unique collections for any style.</p>
          </div>
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <p>From highly-capable sport and professional watches, such as the Submariner, Daytona, and GMT-Master, to
              elegant and classically styled timepieces like the Datejust, Oyster Perpetual, and Day-Date President, the
              brand manufacturers watches that perfectly cater to nearly every possible lifestyle. Bob's Watches is the
              leading online marketplace for luxury timepieces, where customers can buy, sell, and trade popular
              secondhand timepieces at fair market prices.</p>
            <p>2019 Update: For anyone who has been watching closely, the market has been on fire lately with demand
              outstripping supply for a number of models. Genuine watch enthusiasts and collectors have likely seen
              their watches appreciate significantly.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <center>
            <hr>
          </center>
        </div>
      </div>

      <!-- top rolex watches -->
      <div class="col-12 mt-4">
        <div class="text-center">
          <h3 class="text-uppercase">Top Rolex Watches</h3>
        </div>
        <!-- new row -->
        <div class="row py-5">
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <h4 class="text-uppercase">Rolex cosmograph daytona</h4>
            <ul>
              <li>First introduced in 1963 and remaining a part of Rolex's catalog ever since, this iconic sport model
                is the unrivaled tool watch for racing enthusiasts who have a passion for speed and performance. As of
                2020, this legendary model is available in stainless steel, solid gold, and two tone steel and gold. The
                current stainless steel model is the reference 116500LN, which is available with a black or white dial
                and retails for $13,150. However, the watch trades hands for significantly more than its original retail
                price on the secondary market, simply due to demand far exceeding supply and the presence of multi-year
                waiting lists present at a retail level.</li>
            </ul>
          </div>
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <h4 class="text-uppercase">rolex submariner date</h4>
            <ul>
              <li>This ultimate dive watch has always been a standout favorite in the brand's lineup and some vintage
                models have recently sold for millions. The current date-displaying model is the reference 126610LN,
                which was released in September 2020 and retails for $9,150.</li>
            </ul>
            <h4 class="text-uppercase">rolex gmt-master ||</h4>
            <ul>
              <li>First launched in 1983, the GMT-Master II is a horology icon and current model maintains its original
                tool feature for displaying two different time zones simultaneously. The current reference 126710BLRO
                (Pepsi) retails for $9,700 in steel.</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <center>
              <hr>
            </center>
          </div>
        </div>

        <!-- how much is a rolex watch? -->
        <div class="col-12">
          <div class="text-center py-3">
            <h3 class="text-uppercase">how much is a rolex watch?</h3>
          </div>
          The price can vary greatly depending on the specific model, the metals/materials selected in its construction,
          the overall condition of the timepiece (and its box/accessories), and whether the watch is being sold as
          secondhand or brand-new. Certain watches can be purchased for as little as a few thousand dollars; however
          others sell for hundreds of thousands – sometimes even millions of dollars, and represent some of the most
          expensive wristwatches in the world.
        </div>

        <!-- table -->
        <div class="col-12 pt-4">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">Model</th>
                  <th scope="col">Reference</th>
                  <th scope="col">Price (approx.)</th>
                  <th scope="col">Type</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Day-Date</th>
                  <td>Ref. 228238</td>
                  <td> 36,550 USD</td>
                  <td>Sporty Dress Watch</td>
                </tr>
                <tr>
                  <th scope="row">Daytona</th>
                  <td>	Ref. 116500LN</td>
                  <td> 	13,150 USD</td>
                  <td>Chronograph</td>
                </tr>
                <tr>
                  <th scope="row">Cellini Date</th>
                  <td>	Ref. 50515</td>
                  <td> 	17,900 USD</td>
                  <td>Dress Watch</td>
                </tr>
                <tr>
                  <th scope="row">Sea-Dweller</th>
                  <td>	Ref. 126600</td>
                  <td>	11,700 USD</td>
                  <td>Deep Sea Dive Watch</td>
                </tr>
                <tr>
                  <th scope="row">Deepsea</th>
                  <td>	Ref. 126660</td>
                  <td> 	12,900 USD</td>
                  <td>	Deep Sea Dive Watch</td>
                </tr>
                <tr>
                  <th scope="row">GMT-Master II</th>
                  <td>	Ref. 126710BLRO</td>
                  <td> 	9,700 USD</td>
                  <td>	GMT Watch</td>
                </tr>
                <tr>
                  <th scope="row">Submariner Date</th>
                  <td>Ref. 126610LN</td>
                  <td>	9,150 USD</td>
                  <td>	Dive Watch</td>
                </tr>
                <tr>
                  <th scope="row">Submariner</th>
                  <td>	Ref. 124060</td>
                  <td> 	8,100 USD</td>
                  <td>	Dive Watch</td>
                </tr>
                <tr>
                  <th scope="row">Milgauss</th>
                  <td>	Ref. 116400GV</td>
                  <td> 	8,300 USD</td>
                  <td>	Anti-magnetic Watch</td>
                </tr>
                <tr>
                  <th scope="row">Datejust</th>
                  <td>	Ref. 126233</td>
                  <td> 	11,700 USD</td>
                  <td>Sporty Dress Watch</td>
                </tr>
                <tr>
                  <th scope="row">Air-King</th>
                  <td>	Ref. 116900</td>
                  <td> 	6,450 USD</td>
                  <td>Pilot's Watch</td>
                </tr>
                <tr>
                  <th scope="row">Explorer</th>
                  <td>Ref. 214270</td>
                  <td> 	6,550 USD</td>
                  <td>Tool Watch</td>
                </tr>
                <tr>
                  <th scope="row">Yacht-Master</th>
                  <td>Ref. 126622</td>
                  <td> 12,000 USD</td>
                  <td>Dressy Sports Watch</td>
                </tr>
                <tr>
                  <th scope="row">Oyster Perpetual 41</th>
                  <td>Ref. 124300</td>
                  <td> 	5,900 USD</td>
                  <td>Sporty Dress Watch</td>
                </tr>
                <tr>
                  <th scope="row">Sky-Dweller</th>
                  <td>Ref. 326238</td>
                  <td>	40,000 USD</td>
                  <td>	Travel Watch</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <center>
              <hr>
            </center>
          </div>
        </div>
        <!-- end of table -->
        <!-- History -->
        <div class="col-12 py-3">
          <div class="text-center">
            <h3 class="text-uppercase">History</h3>
          </div>
        </div>

        <!-- new row -->
        <div class="row py-5">
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <p>For decades, the name has been synonymous with precision, elegance, and class. It is the fitting
              punctuation mark at the end of any successful career, the memento that is passed down from grandfather to
              father to son, and the only piece of jewelry to rival the engagement ring. Sprung from the imagination of
              Hans Wilsdorf and Alfred Davis in 1908, the brand is regarded as the status symbol, a breathtaking display
              of prestige and power.</p>
          </div>
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <h4 class="text-uppercase">craftsmanship</h4>
            <p>These handcrafted Swiss timepieces, featuring the bejeweled face and the second hand that unmistakably
              sweeps without ever ticking, embody exquisite craftsmanship. More than a watch, this timepiece is a
              long-term investment that will last decades and is known to appreciate in price over time.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <center>
              <hr>
            </center>
          </div>
        </div>

        <!-- about the collection -->
        <div class="col-12 py-3">
          <div class="text-center">
            <h3 class="text-uppercase">About The Collection</h3>
          </div>
        </div>

        <!-- new row -->
        <div class="row py-5">
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <p>The brand currently produces about a dozen different Oyster Perpetual watch models. Among the most
              timeless of the available models are:</p>
            <ul>
              <li><b>Submariner</b><br>
                The Oyster Perpetual Submariner. Originally designed for diving and boasting an impressive maximum
                submergible depth of 300 meters, the Submariner can be found on 007's wrist in 11 James Bond movies.
              </li>
              </li>
              <li><b>President Day Date</b><br>
                The President, also known as "El Presidente," this is the Day-Date model – which was the first to
                display the day and date in its entirety – customized with The President bracelet. Named for its first
                recipient, President Dwight D. Eisenhower.
              </li>
              <li><b>Date</b><br>
                The Oyster Perpetual Date is a stylish timepiece that was created as a mid-sized dress watch.
              </li>
              <li><b>Sea-Dweller</b><br>
                The Oyster Perpetual Date Sea-Dweller. This line of watches, designed with professional divers in mind,
                is the most water resistant mechanical watch on the market. Director James Cameron affixed one to the
                DSV Deepsea Challenger as it made its dive into the deepest point of any ocean.
              </li>
              <li><b>Cellini</b><br>
                The Cellini. Named for Benvenuto Cellini, a goldsmith and papal sculptor during the Italian Renaissance,
                this watch is a true classic. This perfect dress watch, which embodies nobility and elegance, is
                available in three different versions, Cellini Date, Cellini Time, and Cellini Dual Time.
              </li>
              <li><b>Daytona</b><br>
                The Oyster Perpetual Cosmograph Daytona. The archetype of the sport watch for racing drivers. The late
                legend Paul Newman received it as a gift from his wife in 1972 and would go on to wear it every day
                until his passing in 2008.
              </li>
            </ul>
          </div>
          <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <ul>
              <li><b>Datejust</b><br>
                The Perpetual DateJust. The first wristwatch to feature a date function, this model served as the basis
                for the Submariner. Chuck Yeager was wearing the Perpetual Date - just when he broke the sound barrier
                in 1947.
              </li>
              <li><b>GMT-Master</b><br>
                The Oyster Perpetual Date GMT Master. Designed in collaboration with Pan Am Airlines for pilots and
                navigators, this model displays two time zones at once. Before the days of GPS, an accurate GMT source
                was a must for aviation planning, weather forecasts, and scheduling.
              </li>
              <li><b>Milgauss</b><br>
                The Oyster Perpetual Milgauss. This line of watches was designed as an antimagnetic watch for those who
                work in places where electromagnetic fields disrupt the timing mechanism of a watch, like research labs
                and power plants. From the Latin words mille and gauss, this watch can withstand a magnetic flux density
                of 1,000 gauss.
              </li>
              <li><b>Air-King</b><br>
                The Air King. One of the longest, continuously manufactured models and among the most affordable – the
                manually-wound "Warrior Watch" was created by Wilsdorf to be utilized by the British Royal Air Force
                during the 1930s. Senator John McCain still proudly wears an Air King.
              </li>
              <li><b>Yacht-master</b><br>
                The Yacht-Master. First released in 1992, this line of sport watches is available in a lady's model and
                is notable for being the first instance releasing a man’s model with smaller dimensions.
              </li>
              <li><b>Sky-Dweller</b><br>
                The Sky-Dweller was released in 2012. This line of watches is the first to offer both dual time zones
                and an annual calendar. The calendar only needs to be reset every fourth year to accommodate Leap Year.
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <center>
              <hr>
            </center>
          </div>
        </div>
        <!-- BROWSE POPULAR WATCHES FOR SALE -->
        <div class="col-12">
          <div class="text-center">
            <h3 class="text-uppercase">BROWSE POPULAR WATCHES FOR SALE</h3>
          </div>
          <!-- new row -->
          <div class="row py-5">
            <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <p>Bob's Watches is the leading retailer to sell or buy watches including the popular Submariner,
                Day-Date, Datejust, and all other models. Great care is taken to price every watch at or below current
                market value as your complete satisfaction is paramount. All inventory on Bob's Watches is in stock and
                available for immediate delivery. Every watch is guaranteed to be </p>
            </div>
            <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <p>a 100% genuine, authentic and comes with a Certified warranty. Please choose from one of the
                second-hand models for sale or visit our Sell Rolex page if you are looking to trade or sell your Rolex.
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>
          </div>

          <!-- Common Rolex Questions -->

          <div class="col-12">
            <div class="text-center">
              <h3 class="text-uppercase">Common Rolex Questions</h3>
            </div>
            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>While the brand is undoubtedly one of the most recognizable in the world, they are notoriously
                  tight-lipped. Naturally, many people have questions and have a hard time finding reputable sources of
                  information. We have spent years researching everything there is to know about the legendary Swiss
                  brand, and we have compiled a list of the most frequently asked questions</p>
                <h4 class="text-uppercase"> WHAT IS THE CHEAPEST ROLEX WATCH?</h4>
                <ul>
                  <li>
                    <b>Prices for brand-new models start out at $5,100 </b> for the Oyster Perpetual: the smallest and
                    least expensive
                    ladies model. Prices quickly increase from there depending on size, model, materials, and additional
                    features/complications.
                    <br>
                    For watches sold on the secondary market, pricing can be a bit more ambiguous; however since the
                    brand is one of the most well known luxury companies on this planet, it is highly unlikely that
                    anyone will ever come across a genuine piece being sold for only a few hundred dollars.
                    Additionally, not all watches are sold in proper working order; and in many instances, it is the
                    watches being sold for the cheapest prices that often require the most expensive repairs. Below,
                    you'll find a list of the least expensive watches in the catalogue:
                    <ul class="px-5">
                      <li>Oyster Perpetual 28mm: $5,100</li>
                      <li>Oyster Perpetual 31mm: $5,200</li>
                      <li>Oyster Perpetual 34mm: $5,300</li>
                      <li>Oyster Perpetual 36mm: $5,600</li>
                      <li>Datejust 31mm: $6,600</li>
                      <li>Air-King: $6,450</li>
                      <li>Lady-Datejust 28mm: $6,500</li>
                      <li>Date 34mm: $6,500</li>
                      <li>Explorer: $6,550</li>
                      <li>Datejust 36mm: $7,050</li>
                    </ul>
                  </li>
                </ul>
                <h4 class="text-uppercase">HOW MUCH IS A USED ROLEX?</h4>
                <ul>
                  <li>
                    Much like brand-new watches, the price of a used model can vary greatly depending on the specific
                    model, its materials, and the overall condition of the timepiece itself. It is also worth noting
                    that secondhand timepieces can often sell for significantly more than brand-new ones due to their
                    age, rarity, and overall desirability. Additionally, it is not uncommon for certain pre-owned
                    watches, which are still currently in production, to sell for more than brand-new examples, simply
                    due to an overwhelming demand and the existence of multi-year-long waitlists at authorized
                    retailers.
                  </li>
                </ul>
                <h4 class="text-uppercase">WHY ARE ROLEX SO EXPENSIVE?</h4>
                <ul>
                  <li>
                    Universally recognized for producing some of the finest and most reliable timepieces available,
                    Rolex only uses the very best materials to create its watches. Precious metals such as 18k gold and
                    950 platinum are inherently expensive, however Rolex also puts the same industry-leading level of
                    precision engineering and attention to detail into every single watch that it creates.
                  </li>
                </ul>
                <h4 class="text-uppercase">IS A ROLEX WATCH A GOOD INVESTMENT? DO THEY HOLD THEIR VALUE?</h4>
                <ul>
                  <li>
                    Universally recognized for producing some of the finest and most reliable timepieces available,
                    Rolex only uses the very best materials to create its watches. Precious metals such as 18k gold and
                    950 platinum are inherently expensive, however Rolex also puts the same industry-leading level of
                    precision engineering and attention to detail into every single watch that it creates.
                  </li>
                </ul>

                <h4 class="text-uppercase">WHERE IS ROLEX MADE?</h4>
                <ul>
                  <li>
                    Rolex watches are made in Switzerland. Rolex is known for manufacturing all of its watches entirely
                    in-house, and even produces some of the raw materials to create its watches from inside its own
                    Switzerland-based foundry.
                  </li>
                </ul>
                <h4 class="text-uppercase">WHAT IS A CERTIFIED PRE-OWNED ROLEX? </h4>
                <ul>
                  <li>
                    A certified pre-owned watch is a timepiece that has been professionally inspected by a third-party
                    certification service for both condition and authenticity. Certified timepieces are issued a
                  </li>
                </ul>
              </div>



              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>report, which attests to their originality/authenticity, and details the condition of the watch’s
                  most important components. In order to be a certified authentic, each watch must go through a
                  comprehensive process that consists of rigorous criteria and meticulously performed procedures.
                  Certified timepieces are guaranteed to be 100% authentic, and every timepiece sold by Bob’s Watches
                  comes with an optional and independent third-party certification report issued by WatchCSA, the
                  leading authority on timepiece certification.</p>
                <h4 class="text-uppercase">HOW TO TELL IF A ROLEX IS REAL</h4>
                <ul>
                  <li>
                    There are a number of different ways to tell if a Rolex is real, but one of the best ways is to look
                    at the details on the watch in question. Rolex watches are some of the finest timepieces on Earth,
                    and they should look like it. If the watch feels cheap or has any sloppy printing, that's a good
                    sign that it may not be a real Rolex. Be sure to check out our full comprehensive guide on How to
                    Spot a Fake Rolex, and if you still are unsure, you can always bring it to your nearest authorized
                    Rolex retailer to confirm.
                  </li>
                </ul>
                <h4 class="text-uppercase">ARE ALL ROLEX WATCHES MADE FROM REAL GOLD?</h4>
                <ul>
                  <li>
                    The short answer is no. Although the brand makes some of the finest and most expensive watches in
                    the world, many of their most famous and desirable offerings are constructed from their patented
                    904L stainless steel, while others are made from solid 950 platinum. On modern models, any
                    components that appear to be gold are actually constructed from solid 18k gold; however many years
                    ago, the company did manufacture watches that were made from stainless steel and covered with gold
                    caps/shells on their outer surfaces to give them the appearance of having an all-gold construction.
                  </li>
                </ul>
                <h4 class="text-uppercase">DO ROLEX TICK?</h4>
                <ul>
                  <li>
                    The vast majority of Rolex watches are powered by mechanical movements. This means that they do not
                    tick once per second like most modern watches. However, Rolex did produce a small handful of
                    battery-powered Quartz watches previously in its history, so this rule does have its exceptions for
                    these 'Oysterquartz' models. Additionally, like other mechanical watches, if a Rolex is left to sit
                    for multiple days it will stop running until it is either worn again or manually wound through the
                    winding crown.
                  </li>
                </ul>
                <h4 class="text-uppercase">HOW TO WIND A ROLEX</h4>
                <ul>
                  <li>
                    Many Rolex watches are equipped with automatic self-winding movements that wind themselves with the
                    natural motion of your arm, however these watches can also be manually wound - similar to how you
                    would on a traditional pocket watch. To wind a Rolex, you unscrew the winding crown and rotate it
                    clockwise. This will manually supply tension to the mainspring, and allow it to run while off your
                    wrist.
                  </li>
                </ul>
                <h4 class="text-uppercase">CAN YOU BUY A ROLEX FOR $1000?</h4>
                <ul>
                  <li>
                    You may find a Rolex for sale at a really cheap price, but you likely won't be able to buy a Rolex
                    for as as little as $1,000. Genuine Rolex watches are highly desirable and the brand is universally
                    known, so when you see one for sale at an unbelievably cheap price, there is a pretty good chance
                    that there is something wrong with it. The watch may require very expensive repairs or even have
                    non-genuine components on it. Always remember: if the deal sounds too good to be true, it probably
                    is.
                  </li>
                </ul>
                <h4 class="text-uppercase">WHAT IS THE BEST ROLEX TO BUY?</h4>
                <ul>
                  <li>
                    The best Rolex to buy comes down to what you want out of your watch. Rolex watches come in all
                    different shapes and sizes and many were built for specific purposes such as SCUBA diving or yacht
                    racing. The best Rolex watch for one person might not be the best for another, and it is most
                    important to find a Rolex model that fits your wrist and works well with your lifestyle.
                  </li>
                </ul>
                <h4 class="text-uppercase">CAN YOU WEAR A ROLEX EVERYDAY?</h4>
                <ul>
                  <li>
                    You can certainly wear a Rolex every single day! Rolex watches make some of the best daily-wear
                    timepieces available and can last multiple lifetimes when properly maintained. Despite being
                    considered luxury items, Rolex watches are incredibly durable and offer their owners many years of
                    reliable performance.
                  </li>
                </ul>

              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <center>
                  <hr>
                </center>
              </div>
            </div>
            <!-- why you shoud buy reolex pre owned -->
            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">WHY YOU SHOULD BUY WOMEN'S OR MEN'S ROLEX PRE-OWNED</h3>
              </div>
            </div>

            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>While the experience of walking into an authorized dealer and purchasing brand-new can be exciting,
                  buying pre-owned is a much smarter and more practical method of securing a timepiece from one of the
                  most important </p>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">

                <p>luxury watch brands in history. Buying from Bob's Watches can be a smart choice if one is considering
                  the purchase as a long-term investment.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <center>
                  <hr>
                </center>
              </div>
            </div>

            <!-- difference between certified -->
            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">THE DIFFERENCE BETWEEN CERTIFIED PRE-OWNED VS USED VS NEW</h3>
              </div>
            </div>

            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>Just like a car, every piece can only be sold as "brand-new" once, and can only be sold as brand-new
                  by an authorized dealer. As soon as the watch leaves the dealership and the warranty card has been
                  filled out, that timepiece is technically pre-owned, even if it is not worn a single time after that.
                </p>
                <p>Even if you purchased brand-new, that watch is now considered pre-owned because you are now the owner
                  of it. Should you wish to sell that same watch – even if you only owned it for a single day, and never
                  wore it once – that watch is technically no longer new, and in the vast majority of instances, will be
                  worth less than what you originally paid for it. While the amount of </p>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>depreciation can vary significantly depending on the model of watch and its use of precious metals,
                  there will (almost) always be some degree of reduction in value that immediately occurs the moment the
                  watch leaves the authorized dealer.</p>
                <p>Although used and certified pre-owned (CPO) might sound similar, the primary distinction is that CPO
                  watches are certified authentic typically by an independent third party. At Bob's Watches, our
                  authenticity pledge guarantees through an independent service provider that each of our pieces are in
                  fact 100% authentic.</p>
              </div>
            </div>
            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>

            <!-- new iframe -->
            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">HOW TO BUY A ROLEX WATCH ONLINE</h3>
                <p>With thousands of 5-star reviews and an A+ rating from the Better Business Bureau, Bob's Watches has
                  become the world's most trusted place to buy and sell online. Don't believe us? See for yourself -
                  click the link below to read through our reviews.</p>
              </div>
            </div>

            <div class="col-12">
              <ul>
                <li>4.8/5 Star Rating from 700+ Verified Reviews</li>
                <li>A+ Better Business Bureau</li>
              </ul>
              <div class="embed-responsive embed-responsive-4by3">
                <iframe class="embed-responsive-item rolex-video" src="https://youtu.be/Tn9nlw8jFN4"
                  allowfullscreen></iframe>
              </div>
            </div>
            <div class="row">

            </div>

            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">WHAT SHOULD I LOOK FOR WHEN BUYING A ROLEX WATCH ONLINE?</h3>
              </div>
            </div>
            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>When you buy Rolex watches, like the Rolex Submariner, it is important you find a reputable dealer to
                  ensure that you are in fact purchasing a genuine, 100% authentic. This is and should be more important
                  than the price. At Bob’s Watches, we guarantee the authenticity of each and every watch we sell. In
                  fact, most of our watches have been bought directly from the prior owner and come with the original
                  box and paperwork from Rolex USA.</p>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>Every watch listed on our website comes backed by our lifetime authenticity pledge and is certified
                  to be 100% authentic by a third party organization called Watch CSA. You can rest assured knowing that
                  one of our expert watch makers has thoroughly inspected every detail before a wristwatch is listed for
                  sale.</p>
              </div>
            </div>

            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>
            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">IS THE PROCESS OF BUYING A WATCH SECURE?</h3>
              </div>
            </div>
            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>At Bob's Watches we understand that guarding your privacy and information is a top priority. So
                  please know that when you purchase a Rolex from us, the transaction is protected by the highest level
                  of security in a 256-bit SSL (or Secure Socket Layer) Encryption. Some online shopping websites only
                </p>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>offer 128-bit security. This is yet another important reason to trust Bob's Watches the next time you
                  buy a Rolex watches online.</p>
              </div>
            </div>
            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>
            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">HOW CAN I PAY FOR MY ROLEX WATCH?</h3>
              </div>
            </div>
            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>We make the process of purchasing your Rolex watch easy by offering our customers a multitude of ways
                  to pay for their watch. Whether you want to pay by credit card, financing or by wire transfer, we can
                  easily accommodate your request. We do this because we know people have different preferences when
                  purchasing such expensive, precious items and we want </p>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>to make sure every customer can do what they feel comfortable with. Not only do we go the extra mile
                  to offer our customers a wealth of different payment options, we offer every customer complimentary
                  overnight shipping.</p>
              </div>
            </div>

            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>

            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">TOP STORIES - JANUARY 2021</h3>
              </div>
            </div>
            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <h4>Rolex Inventory is Still Scare at Authorized Dealers:</h4>
                <ul>
                  <li>Despite resuming production after shuttering their factory doors in March of 2020 due to COVID-19,
                    Rolex continues to struggle to keep up with the demand that seems to have surged since pandemic
                    restrictions were lifted.</li>
                  <li>The Global supply of Rolex was already an issue that already existed prior to the temporary
                    shutdown in March, and the temporary factory closures only made supply even more scarce.</li>
                  <li>Strict safety regulations during most of 2020 resulted in the loss of approximately one quarter of
                    Rolex’s projected inventory for the year, and the effects of this are certainly being felt at a
                    retail level all around the world.</li>
                </ul>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <h4>Rolex Named the UK's Top Brand:</h4>
                <ul>
                  <li>Inventory issues aside, Rolex sales soared overseas. Rolex dethroned Lego and beat out other
                    brands Visa, Samsung, and Andrex for the coveted title of top UK brand.</li>
                </ul>
                <h4>Spring Trade Fair in Geneva Cancelled:</h4>
                <ul>
                  <li>Instead of presenting their newest offerings at the Geneva exhibition center alongside Watches and
                    Wonders, major brands Rolex Patek Philippe, Tudor, Chanel, and Chopard, have opted to host a digital
                    showcase to take place April 7th - April 13th.</li>
                </ul>
              </div>
            </div>

            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>

            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">TRENDING MODELS - JANUARY 2021</h3>
              </div>
            </div>
            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>The new Rolex Submariner is the hottest watch to hit the market in recent months. Featuring a 41mm
                  case and the next-generation Caliber 3235 movement, it isn’t hard to see why it has collectors
                  scrambling to their authorized dealers to get their hands on one.</p>
                <p>The lineup includes:</p>
                <ul class="px-5">
                  <li><b>126618LB & 126618LN </b>– 18k yellow gold.</li>
                  <li><b>126613LB & 126613LN</b>– Yellow Rolesor.</li>
                  <li><b>126610LV </b>- Oystersteel/Green Bezel</li>
                  <li><b>126610LN </b>– Oystersteel/Date</li>
                  <li><b>124060 </b>– Oystersteel/No Date (Caliber 3230)</li>
                </ul>
                <p>What's the buzz about, anyway? The Cal. 3235 perpetual movement has been in production for a few
                  years now. However, 2020 marks the first year that the Submariner series has ever been fitted with
                  this powerhouse </p>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>movement. It boasts Rolex's proprietary Chronergy escapement and a new self-winding module, both of
                  which help contribute to its longer 70-hour power reserve.</p>
                <p>The 2020 Rolex Submariner release means that the 1166xx generation has since been discontinued and is
                  currently trending on the pre-owned market. One such Rolex is the ref. 116610LV - aka the "Hulk" -
                  with its green ceramic bezel and matching green sunburst Maxi dial. Officially unavailable at
                  retailers, now is the time to invest if you've always had your eye on this stunning all-green
                  Submariner.</p>
              </div>
            </div>

            <div class="col-12">
              <center>
                <hr>
              </center>
            </div>
            <div class="col-12 py-3">
              <div class="text-center">
                <h3 class="text-uppercase">THE BOB'S WATCHES WARRANTY</h3>
              </div>
            </div>
            <!-- new row -->
            <div class="row py-5">
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>Bob's Watches offers a one year warranty on all the Rolex watches we sell. If your watch stops
                  working during this period, just send it back and we will have it repaired at no cost to you. We also
                  offer a 3-day return policy which </p>
              </div>
              <div class="col-xxl-6 col-lx-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <p>allows you to send the watch back to us for any reason. Because your complete satisfaction is our
                  primary concern, our customers feel secure knowing they have this guarantee.</p>
              </div>
            </div>




















          </div>
        </div>
  </section>

  <?php include('footer.php'); ?>